//Google maps (declaration)
var map;
var oms;
var markersArray = [];
var minZoomLevel = 2;
var markerClusterer = null;
//Default location 
var center = new google.maps.LatLng(15, 15);

// Executes when DOM is fully loaded. 
$(document).ready(function() {
    //Map options
    var mapOptions = {
        zoom: minZoomLevel,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    //Declares map
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    oms = new OverlappingMarkerSpiderfier(map);

    // Limit the zoom level
    google.maps.event.addListener(map, 'zoom_changed', function() {
        if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
    });

    //  Dummie
    var jsonUrl = "attended.json";

    //AJAX call to get JSON from server
    $.getJSON(jsonUrl, function(res) {
        //Adds marker using information from JSON
        addMarkerfromJson(res);
    });
});

//Parses JSON information to call geocoder, concatenating city and venue values, for accuracy

function addMarkerfromJson(data) {
    var setlists = data.setlists.setlist;
    var concerts = [];

    for (var i = 0, l = setlists.length; i < l; i++) {
        var valid = setlists[i].sets.set;
        if (!valid) continue;
        var concert = {
            tour: setlists[i]["@tour"],
            artist: setlists[i].artist["@name"],
            date: setlists[i]["@eventDate"],
            song1: setlists[i].sets.set[0].song[0]["@name"],
            song2: setlists[i].sets.set[0].song[1]["@name"],
            song3: setlists[i].sets.set[0].song[2]["@name"],
            setlistURL: setlists[i].url,
            cityName: setlists[i].venue.city["@name"],
            cityLatitude: setlists[i].venue.city.coords["@lat"],
            cityLongitude: setlists[i].venue.city.coords["@lng"],
            countryName: setlists[i].venue.city.country["@name"],
            venueName: setlists[i].venue["@name"]
        };
        concerts.push(concert);
    }

    for (var j = 0, x = concerts.length; j < x; j++) {
        var infoWindow = createInfoWindow(concerts[j].artist, concerts[j].venueName, concerts[j].tour, concerts[j].date, concerts[j].song1, concerts[j].song2, concerts[j].song3, concerts[j].setlistURL, concerts[j].cityName);
        getPlace(concerts[j].venueName, concerts[j].cityName, concerts[j].countryName, concerts[j].cityLatitude, concerts[j].cityLongitude, infoWindow);
    }
}

function getPlace(venueName, cityName, countryName, cityLatitude, cityLongitude, infoWindowText) {

    var jsonUrl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + venueName + "+" + cityName + "+" + countryName +"&location=" + cityLatitude + "," + cityLongitude + "&radius=5000&sensor=false&key=AIzaSyDNnzKWzr3hrXzzEFmwgIQ7-9-qrGyZnXM";

    //AJAX call to get JSON from server
    $.getJSON(jsonUrl, function(data) {
        var place = data.results[0];

        //Adds marker using information from JSON
        var venueLocation = new google.maps.LatLng(place.geometry.location.lat, place.geometry.location.lng);
        addMarker(venueLocation, infoWindowText);
    });
}

//Adds marker and fills marker array, marker cluster and calls infowindows builder

function addMarker(location, infoWindowText) {
      var marker = new google.maps.Marker({
        position: location,
        map: map
      });
      attachIWindow(marker, infoWindowText);
      oms.addMarker(marker);
      markersArray.push(marker);
      refreshMap();
}

//Takes care of clustering markers

function refreshMap() {
    if (markerClusterer !== null) {
        markerClusterer.clearMarkers();
    }
    var zoom = 10; 
    markerClusterer = new MarkerClusterer(map, markersArray, {
        maxZoom: zoom
    });
}

//Builds infowindows using information from JSON

function attachIWindow(marker, content) {

    var infowindow = new google.maps.InfoWindow({
        maxWidth: 400,
        content: content
    });

    google.maps.event.addListener(marker, 'click', function() {
        map.setCenter(marker.getPosition());
        infowindow.open(map, marker);
    });
}

//Builds infowindows based on data from JSON

function createInfoWindow(artist, venue, tour, date, song1, song2, song3, setlistURL, cityName) {

    var contentStr;
    var queryStr = artist + ' ' + venue + ' ' + date;
    //console.log(queryStr, typeof queryStr);

    contentStr = '<div class="iwContainer">';
    contentStr += '<div align="left">';
    contentStr += '<h3>' + artist + '</h3><br>';
    contentStr += '<h4>' + tour + '</h4><br>';
    contentStr += '<h6>' + date + ' @ ' + venue + ', '+ cityName +'</h6></div>';
    contentStr += song1 + '<br>';
    contentStr += song2 + '<br>';
    contentStr += song3 + '<br>';
    contentStr += '<a href="' + setlistURL + '" target="_blank">view full setlist</a><br>';
    contentStr += "<a href='#' onclick='GetContent(\"" + queryStr + "\", \"search\");'>view Youtube videos</a><br>";
    contentStr += "<a href='#' onclick='getFlickr(\"" + artist + "\", \"" + cityName + "\");'>view Flickr pictures</a><br>";
    contentStr += '</div>';

    return contentStr;
}

function GetContent(foreign_id, view_type) {
    //var default_id = "itpdwd";  Define default YouTube username ID here
    var max_videos = 2; // How many to show

    if (!foreign_id) {
        foreign_id = default_id;
        view_type = "id";
    }

    // Compute the request URL (either by username or by search term)
    //  Note: "alt=json-in-script&callback=?" params are in URL to allow cross-domain JSON using jQuery
    //  See: http://docs.jquery.com/Ajax/jQuery.getJSON#urldatacallback
    var url;
    if (view_type == "id") url = "http://gdata.youtube.com/feeds/api/users/" + escape(foreign_id) + "/uploads?max-results=" + max_videos +
        "&alt=json-in-script&callback=?";
    else url = "http://gdata.youtube.com/feeds/api/videos?vq=" + escape(foreign_id) + "&max-results=" + max_videos +
        "&alt=json-in-script&callback=?";

    // Clear the HTML display
    document.getElementById("videos").innerHTML = "";

    // Get the data from the web service and process
    $.getJSON(url, function(data) {
        var results = data.feed.entry;
        if (!results) return;
        for (var i = 0, l = results.length; i < l; i++) {
            // Extract video ID from the videos API ID
            var item = results[i];
            var api_id = item.id.$t;
            api_id.match(/\/(\w+?)$/);
            var id = RegExp.$1;

            var title = item.title.$t;

            // Get first 10 chars of date_taken, which is the date: YYYY-MM-DD
            var date_pub = item.published.$t.substring(0, 10);

            // Collect the authors
            var author_text = "";
            for (var j = 0, x = item.author.length; j < x; j++) {
                var item2 = item.author[j];
                if (author_text) author_text += ", ";
                author_text += '<a href="' + item2.uri.$t + '">' + item2.name.$t + '</a>';
            }
            // $.each(item.author, function(j, item2) {});

            // Format the HTML for this video
            var text = '<div align="left" class="video">' +
                '<b class="title">' + title + '</b><br/>' +
                'Published: ' + date_pub + ' by ' + author_text + '<br/>' +
                '<object width="120" height="90"><param name="movie" value="http://www.youtube.com/v/' + id + '&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><embed src="http://www.youtube.com/v/' + id + '&hl=en&fs=1" type="application/x-shockwave-flash" allowfullscreen="true" width="160" height="110"></embed></object></div>';

            // Now append to the HTML display
            $(text).appendTo("#videos");
        }
        // $.each(data.feed.entry, function(i, item) {});
    });
}

function getFlickr(artist, cityName) {

    var tags = artist + ', ' + cityName;
    var url = "http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=?";
    var src;

    $.getJSON(url, {
        tags: tags,
        tagmode: "all",
        format: "json"
    },

   function(data) {
        jQuery('#images').empty();
       $.each(data.items, function(i, item) {
           $("<img/>").attr("src", item.media.m).appendTo("#images");
           document.getElementById('titel').innerHTML = "flickr - pictures of: " + "<b style='color:red;'>" + data.items[i].title + "</b>";
           if (i == 5) return false;
       });
   });
}